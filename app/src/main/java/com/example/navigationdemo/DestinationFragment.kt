package com.example.navigationdemo


import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavDeepLinkBuilder
import kotlinx.android.synthetic.main.fragment_destination.*

class DestinationFragment : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
      sendNotification()
    }

    private fun sendNotification() {
        val arguments = Bundle().apply {
            putString("aStringKey", "a string value")
            putInt("anIntKey", 0)
        }

        val notificationManager =
            context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(
                NotificationChannel(
                    "deeplink", "Deep Links", NotificationManager.IMPORTANCE_HIGH)
            )
        }

// Prepare the pending intent, while specifying the graph and destination
        val pendingIndent = NavDeepLinkBuilder(context!!)
            .setGraph(R.navigation.nav_graph)
            .setDestination(R.id.mainFragment)
            .setArguments(arguments)
            .createPendingIntent()

// Create notification instance
        val builder = NotificationCompat.Builder(
            context!!, "deeplink")
            .setContentTitle("Navigation")
            .setContentText("Deep link to Android")
            .setSmallIcon(R.drawable.navigation_empty_icon)
            .setContentIntent(pendingIndent)
            .setAutoCancel(true)

// Display notification
        notificationManager.notify(0, builder.build())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_destination, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id: Int = DestinationFragmentArgs.fromBundle(arguments!!).id // id = 10
        val data: String = DestinationFragmentArgs.fromBundle(arguments!!).nameToShow // data = "Any data"

        //val name = DestinationFragmentArgs.fromBundle(arguments).nameToShow
        welcomeWithNameTv.text = "Hello $data!"
        notification.setOnClickListener(this)
    }

}
